package com.ispu.weather.structure.layer;

public class Layer {
    private double[] neurons;
    private double[][] weights;
    public double mu = 0.01;

    public Layer(int size, int nextSize) {
        neurons = new double[size];
        weights = new double[size][nextSize];
    }

    public void setNeurons(double[] values) {
        for (int i = 0; i < neurons.length; i++) {
            neurons[i] = values[i];
        }
    }

    public void setNeurons(double value, int index) {
        neurons[index] = value;
    }

    public double[] getNeurons() {
        double[] results = new double[neurons.length];
        for (int i = 0; i < neurons.length; i++) {
            results[i] = neurons[i];
        }
        return results;
    }

    public double getNeurons(int index) {
        return neurons[index];
    }

    public double[][] getWeights() {
        double[][] results = new double[weights.length][weights[0].length];
        for (int i = 0; i < results.length; i++) {
            for (int j = 0; j < results[i].length; j++) {
                results[i][j] = weights[i][j];
            }
        }
        return results;
    }

    public double getWeights(int index_1, int index_2) {
        return weights[index_1][index_2];
    }

    public void setWeights(double value, int index_1, int index_2) {
        weights[index_1][index_2] = value;
    }

    public void setWeights(double[][] weights) {
        this.weights = weights;
    }

}

package com.ispu.weather.structure;

import com.ispu.weather.function.IActivationFunction;
import com.ispu.weather.model.WeatherInput;
import com.ispu.weather.model.WeatherInputVariables;
import com.ispu.weather.structure.layer.Layer;
import com.ispu.weather.utils.Utils;

import java.io.IOException;

public class Network {
    private Layer[] layers;
    private IActivationFunction activationFunction;

    public Network(IActivationFunction activationFunction, int... sizes) throws Exception {
        if(sizes.length == 0) throw new Exception("0 arguments");
        this.activationFunction = activationFunction;
        layers = new Layer[sizes.length];
        for (int i = 0; i < sizes.length; i++) {
            int nextSize = i < sizes.length - 1 ? sizes[i + 1] : 1;
            layers[i] = new Layer(sizes[i], nextSize);
            for (int j = 0; j < layers[i].getNeurons().length; j++) {
                for (int k = 0; k < nextSize; k++) {
                    layers[i].setWeights(Math.abs(Math.random() * 2.0 - 1.0), j, k);
                }
            }
        }
    }

    private void ForwardSignalDist(WeatherInputVariables weatherInputVariables) {
        double[] enteredValues = Utils.fromDouble(weatherInputVariables.getValues().toArray(new Double[0]));
        layers[0].setNeurons(enteredValues);
        for (int i = 1; i < layers.length; i++) {
            for (int j = 0; j < layers[i].getNeurons().length; j++) {
                double sum = 0;
                for (int k = 0; k < layers[i - 1].getNeurons().length; k++) {
                    sum += layers[i - 1].getWeights()[k][j] * layers[i - 1].getNeurons()[k];
                }
                double outputValue = activationFunction.exActivationFunction(sum, 1);
                layers[i].setNeurons(outputValue, j);
            }
        }
    }

    private void BackPropagation(WeatherInputVariables weatherInputVariables) {
        double[] delta = new double[layers[layers.length - 1].getNeurons().length];
        double[] expectations = Utils.fromDouble(weatherInputVariables.getExpectations().toArray(new Double[0]));
        for (int i = 0; i < delta.length; i++) {
            delta[i] = expectations[i] - layers[layers.length - 1].getNeurons()[i];
        }
        for (int i = layers.length - 2; i >= 0; i--) {
            SetLayersWeights(layers[i], delta);
            delta = GetDelta(layers[i], delta);
        }
    }

    private void SetLayersWeights(Layer layer, double[] delta) {
        for (int i = 0; i < layer.getNeurons().length; i++) {
            for (int j = 0; j < delta.length; j++) {
                layer.setWeights(layer.getWeights(i, j) + layer.mu * layer.getNeurons(i) * delta[j], i, j);
            }
        }
    }

    private double[] GetDelta(Layer layer, double[] nextLayerDelta) {
        double[] delta = new double[layer.getNeurons().length];
        for (int i = 0; i < layer.getNeurons().length; i++) {
            double sum = 0.0;
            for (int j = 0; j < nextLayerDelta.length; j++) {
                sum += layer.getWeights(i, j) * nextLayerDelta[j];
            }
            delta[i] = layer.getNeurons(i) * (1 - layer.getNeurons(i)) * sum;
        }
        return delta;
    }

    public void TeachNetwork(WeatherInput weatherInput) {
        for (WeatherInputVariables weatherInputVariables: weatherInput.getWeatherInputVariables()) {
            ForwardSignalDist(weatherInputVariables);
            BackPropagation(weatherInputVariables);
        }
        try {
            Utils.saveWeights(this.layers);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private double func(double x) {
        double b = 1;
        double a = 0;
        double xmin = 1;
        double xmax = 7;
        return (x - xmin) * (b - a) / (xmax - xmin) + a;
    }
    private double nfunc(double x) {
        double b = 1;
        double a = 0;
        double xmin = 1;
        double xmax = 7;
        return xmin + ((x - a) * (xmax - xmin)) / (b - a);
    }

    public void Test(WeatherInput weatherInput) {
        try {
            this.layers = Utils.loadWeights();
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (WeatherInputVariables weatherInputVariables: weatherInput.getWeatherInputVariables()) {
            for (int i = 0; i < weatherInputVariables.getValues().size(); i++) {
                weatherInputVariables.getValues().set(i, func(weatherInputVariables.getValues().get(i)));
            }
            for (int i = 0; i < weatherInputVariables.getExpectations().size(); i++) {
                weatherInputVariables.getExpectations().set(i, func(weatherInputVariables.getExpectations().get(i)));
            }
        }
        //TeachNetwork(weatherInput);
        ForwardSignalDist(weatherInput.getWeatherInputVariables().get(0));
    }
}

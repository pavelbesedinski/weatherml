package com.ispu.weather.structure.neuron;

import com.ispu.weather.function.IActivationFunction;

public interface INeuron {
    void setData(double value, int index);
    double getData(IActivationFunction activationFunction, double bound);
}

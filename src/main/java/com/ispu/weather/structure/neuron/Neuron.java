package com.ispu.weather.structure.neuron;

import com.ispu.weather.function.IActivationFunction;

public class Neuron implements INeuron {
    private double weights[];
    private double weightsSum;

    public Neuron(int countOfWeights) {
        weights = new double[countOfWeights];
        weightsSum = 0.0;
    }

    @Override
    public void setData(double value, int index) {
        weightsSum += value * weights[index];
    }

    @Override
    public double getData(IActivationFunction activationFunction, double bound) {
        return activationFunction.exActivationFunction(weightsSum, bound);
    }

    public double updateWeights(double[] delta, double step) {
        return 0;
    }
}

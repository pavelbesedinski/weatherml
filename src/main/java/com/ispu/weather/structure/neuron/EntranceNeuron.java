package com.ispu.weather.structure.neuron;

import com.ispu.weather.function.IActivationFunction;

public class EntranceNeuron implements INeuron {
    double value;

    @Override
    public void setData(double value, int index) {
        this.value = value;
    }

    @Override
    public double getData(IActivationFunction activationFunction, double bound) {
        return value;
    }
}

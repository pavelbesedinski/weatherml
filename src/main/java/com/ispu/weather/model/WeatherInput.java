package com.ispu.weather.model;

import java.util.ArrayList;

public class WeatherInput {
    private ArrayList<WeatherInputVariables> weatherInputVariables;

    public ArrayList<WeatherInputVariables> getWeatherInputVariables() {
        return weatherInputVariables;
    }

    public void setWeatherInputVariables(ArrayList<WeatherInputVariables> weatherInputVariables) {
        this.weatherInputVariables = weatherInputVariables;
    }
}

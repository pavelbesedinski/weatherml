package com.ispu.weather.model;

import java.util.ArrayList;

public class WeatherInputVariables {
    private ArrayList<Double> values;
    private ArrayList<Double> expectations;

    public ArrayList<Double> getValues() {
        return values;
    }

    public void setValues(ArrayList<Double> values) {
        this.values = values;
    }

    public ArrayList<Double> getExpectations() {
        return expectations;
    }

    public void setExpectations(ArrayList<Double> expectations) {
        this.expectations = expectations;
    }
}

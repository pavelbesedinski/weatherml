package com.ispu.weather.model;

import java.util.ArrayList;

public class WeatherNetwork {
    private ArrayList<WeatherLayer> weatherLayers;

    public ArrayList<WeatherLayer> getWeatherLayers() {
        return weatherLayers;
    }

    public void setWeatherLayers(ArrayList<WeatherLayer> weatherLayers) {
        this.weatherLayers = weatherLayers;
    }
}

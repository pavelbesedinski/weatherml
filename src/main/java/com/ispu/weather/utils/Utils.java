package com.ispu.weather.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ispu.weather.model.WeatherLayer;
import com.ispu.weather.model.WeatherNetwork;
import com.ispu.weather.structure.layer.Layer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Utils {
    private static String SAVE_FOLDER_NAME = "saves/";
    private static String WEIGHTS_FILE_NAME = "weights.json/";

    public static double[] fromDouble(Double[] array) {
        double[] values = new double[array.length];
        for (int i = 0; i < array.length; i++) {
            values[i] = array[i];
        }
        return values;
    }

    public static void saveWeights(Layer[] layers) throws IOException {
        File saveFolder = new File(SAVE_FOLDER_NAME);
        if (saveFolder.exists() == false) {
            saveFolder.mkdir();
        }
        ObjectMapper objectMapper = new ObjectMapper();

        ArrayList<WeatherLayer> layerArrayList = new ArrayList<>();
        for (int i = 0; i < layers.length; i++) {
            WeatherLayer weatherLayer = new WeatherLayer();
            weatherLayer.setId(i);
            weatherLayer.setMu(layers[i].mu);
            weatherLayer.setSize((layers[i].getNeurons().length));
            weatherLayer.setWeights(layers[i].getWeights());

            layerArrayList.add(weatherLayer);
        }
        WeatherNetwork weatherNetwork = new WeatherNetwork();
        weatherNetwork.setWeatherLayers(layerArrayList);

        objectMapper.writeValue(new File(SAVE_FOLDER_NAME + WEIGHTS_FILE_NAME), weatherNetwork);
    }

    public static Layer[] loadWeights() throws Exception {
        File saveFolder = new File(SAVE_FOLDER_NAME);
        if (saveFolder.exists() == false) {
            throw new Exception("Folder " + SAVE_FOLDER_NAME + " does not exist!");
        }
        else {
            System.out.println("Веса загружены!");
        }
        ObjectMapper objectMapper = new ObjectMapper();
        String text = readUsingBufferedReader(SAVE_FOLDER_NAME + WEIGHTS_FILE_NAME);
        WeatherNetwork weatherNetwork = objectMapper.readValue(text, WeatherNetwork.class);
        Layer[] results = new Layer[weatherNetwork.getWeatherLayers().size()];
        for (int i = 0; i < results.length; i++) {
            results[i].setWeights(weatherNetwork.getWeatherLayers().get(i).getWeights());
            results[i].mu = weatherNetwork.getWeatherLayers().get(i).getMu();
        }
        return results;
    }

    private static String readUsingBufferedReader(String fileName) throws IOException {
        BufferedReader reader = new BufferedReader( new FileReader(fileName));
        String line = null;
        StringBuilder stringBuilder = new StringBuilder();
        String ls = System.getProperty("line.separator");
        while( ( line = reader.readLine() ) != null ) {
            stringBuilder.append( line );
            stringBuilder.append( ls );
        }

        stringBuilder.deleteCharAt(stringBuilder.length()-1);
        return stringBuilder.toString();
    }
}

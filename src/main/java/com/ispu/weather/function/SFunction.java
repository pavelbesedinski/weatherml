package com.ispu.weather.function;

public class SFunction implements IActivationFunction {
    @Override
    public double exActivationFunction(double value, double bound) {
        return 1.0 / (1.0 + Math.exp(-value / bound));
    }
}

package com.ispu.weather.controller;

import com.ispu.weather.function.SFunction;
import com.ispu.weather.model.WeatherInput;
import com.ispu.weather.structure.Network;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
@RequestMapping("weather")
public class Weather {

    @RequestMapping(value = "/teach", method = RequestMethod.POST)
    public ResponseEntity<Integer> getTeachWeather(@RequestBody @Valid WeatherInput weatherInput)  {
        try {
            Network network = new Network(new SFunction(), 7, 3, 1);
            network.TeachNetwork(weatherInput);
        } catch (Exception e) {
            e.printStackTrace();
            ResponseEntity.badRequest();
        }
        return ResponseEntity.ok(100);
    }

    @RequestMapping(value = "/get", method = RequestMethod.POST)
    public ResponseEntity<Integer> getWeather(@RequestBody @Valid WeatherInput weatherInput)  {
        try {
            Network network = new Network(new SFunction(), 7, 3, 1);
            network.Test(weatherInput);
        } catch (Exception e) {
            e.printStackTrace();
            ResponseEntity.badRequest();
        }
        return ResponseEntity.ok(100);
    }
}
